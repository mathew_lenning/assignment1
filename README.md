# Welcome to BookFace #

I enjoyed working on this little project. \
This was my first Laravel app, and I think I might be in love.

Since I honed my programming skills in the insanity that is the Joomla Framework,
It was a nice change to work with a system works as expected. 
Plus the documentation is awesome.

### How to deploy the app ###
Since I only used native Laravel and CDN resources for the front-end, you can follow the 
instructions from https://laravel.com/docs/8.x/deployment page to get it up and running.

There is a copy of the database with some test data in the repo root,
but you could run artisan migrate to generate it without the test data.
